$.get( "http://university.netology.ru/api/currency", function( data ) {
    
    $.each(data, function(key, object){
        $('.currency').append($('<option></option>').attr('value', object.Value).html(object.Name))
    })
    
    $.each($('.currency'), function(key, select){
        select.selectedIndex = Math.floor(Math.random() * select.children.length);
    })
    
    $('.sum:first').val(Math.floor(Math.random() * 100));
    
    $('.sum:last').val(($('.currency:first').val() * $('.sum:first').val() / $('.currency:last').val()).toFixed(2));
    
    
});

$('.sum').on('keyup', function(){
    $('form').find('.sum').not($(this)).val( 
        ($(this).prevAll('select:first').val() * $(this).val() / $('form').find('.sum').not($(this)).prevAll('select:first').val())
        .toFixed(2)
    );
})

$('.currency').on('change', function(){
    $('form').find('.sum').not($(this).nextAll('input:first')).val(($(this).val() * $(this).nextAll('input:first').val() / $('form').find('.currency').not($(this)).val()).toFixed(2));
})